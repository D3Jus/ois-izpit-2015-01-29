var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	if(req.query.uporabniskoIme != "" && req.query.geslo != "") {
		if(preveriDatotekaStreznik(req.query.uporabniskoIme, req.query.geslo) || preveriSpomin(req.query.uporabniskoIme, req.query.geslo)) {
			res.send({status: true, napaka: ""});
		} else {
			res.send({status: false, napaka: "Avtentikacija ni uspela."});
		} 
	} else {
		res.send({status: false, napaka: "Napačna zahteva."});
	}
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	if(preveriDatotekaStreznik(req.query.uporabniskoIme, req.query.geslo) || preveriSpomin(req.query.uporabniskoIme, req.query.geslo)) {
		res.send("<html><title>Uspešno</title><body><p>Uporabnik <b>" + req.query.uporabniskoIme + "</b> uspešno prijavljen v sistem!</p></body></html>");
	} else {
		res.send("<html><title>Napaka</title><body><p>Uporabnik <b>" + req.query.uporabniskoIme + "</b> nima pravice prijave v sistem!</p></body></html>");
	}
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync(__dirname + "/public/podatki/uporabniki_streznik.json").toString());


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	for(var i=0; i < podatkiSpomin.length; i++) {
		var data = podatkiSpomin[i].split("/");
		
		if(uporabniskoIme == data[0] && geslo == data[1]) {
			return true;
		}
	}
	return false;
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for(var i=0; i < podatkiDatotekaStreznik.length; i++) {
		if(podatkiDatotekaStreznik[i].uporabnik == uporabniskoIme && podatkiDatotekaStreznik[i].geslo == geslo) {
			return true;
		}
	}

	return false;
	
}